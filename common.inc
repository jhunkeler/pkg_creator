function readlink_ex
{
    script_location=
    FAILURE=0
    if [ -e "$(which readlink 2>/dev/null)" ]; then
        export script_location=$(dirname `readlink -f "$1"`)
        retval=$?
        if [ $retval -ne 0 ]; then
            export FAILURE=1
        fi
    else
        export FAILURE=0
    fi

    # Well, if readlink doesn't work we only have one more option (i.e that I care to implement)
    if [ $FAILURE -ne 0 ];then 
        export FAILURE=0
        export script_location=$(python -c "from __future__ import print_function; import os; print('{0}'.format(os.path.abspath(os.curdir)));")
        retval=$?
        if [ $retval -ne 0 ]; then
            export FAILURE=1
        fi
    fi 

    if [ $FAILURE -ne 0 ]; then
        echo "readlink failure. abort." 
        exit 1
    fi
    
    echo $script_location 
}

function init_pkgbuild
{
    PKGBUILD_CMD="pkgbuild"
    PKGBUILD_ARGS="--scripts $PKG_SCRIPTS --identifier edu.stsci.$PKG_NAME.pkg --root $PKG_ROOT"
    export PKGBUILD_CMD
    export PKGBUILD_ARGS
}

function init_vars
{
    scripts_default=( preinstall postinstall )
	export scripts_default
}
